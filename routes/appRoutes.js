 'use strict';

module.exports = function(app) {
  var cebecKids = require('../controllers/appControllers');

  // cebecKids Routes
  app.route('/families')
    .get(cebecKids.listFamilies)
    .post(cebecKids.addFamily);


 

  app.route('/families/:idFamily')
  	.get(cebecKids.getFamily)
  	.put(cebecKids.updateFamily)
  	.delete(cebecKids.deleteFamily);


  app.route('/kids')
    .get(cebecKids.listKids)
    .post(cebecKids.addKid);



  app.route('/kids/:idKid')
  	.get(cebecKids.getKid)
    .put(cebecKids.updateKid)
    .delete(cebecKids.deleteKid); 

  app.route('/assistence')
    .post(cebecKids.addAssistence) 
    .get(cebecKids.getAssistence);

};
