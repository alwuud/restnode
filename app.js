const express = require('express');
var app = express();
const bodyparser = require('body-parser');

app.enable('trust proxy');

app.use(bodyparser.json());

var routes = require('./routes/appRoutes'); //importing route
routes(app); //register the route


// Start the server
const PORT =  8080;
app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
// [END gae_flex_quickstart]


/**

app.get( '/kids/:id', 

    (req, res) => {

        mysqlConnection.query('SELECT idKid, names_, lasts_ FROM kid WHERE idKid=?', [req.params.id] ,

            (err, rows, fields)=> {
                if (!err)
                    res.send(rows);
                else
                    console.log(err);

            }

        )
    }


); 


app.get('/kids', 

    (req, res) => {

        let qr= req.query.qr;

        if(!qr)
            mysqlConnection.query('SELECT idKid, names_, lasts_ FROM kid', (err, rows, fields) => {
                if (!err)
                    res.send(rows);
                else
                    console.log(err);
            })
        else 
            mysqlConnection.query('SELECT idKid, names_, lasts_ FROM kid INNER JOIN \
                family on kid.family_id = family.idFamily and family.qrCode= ?', [qr] ,

                (err, rows, fields)=> {
                    if (!err)
                        res.send(rows);
                    else
                        console.log(err);

                }

            )

    }
);

  **/


app.get('/', (req, res) => {
  res
    .status(200)
    .send('Hello, world!')
    .end();
}); 