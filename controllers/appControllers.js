const mysql = require('mysql');

var mysqlConnection = mysql.createConnection({
    //socketPath: '/cloudsql/cebecinf:us-east1:kidsdb',
    host: '127.0.0.1',
    user: 'root',
    password: 'password',
    database: 'kids',
    multipleStatements: true
});



mysqlConnection.connect((err) => {
    if (!err)
        console.log('DB connection succeded.');
    else
        console.log('DB connection failed \n Error : ' + JSON.stringify(err, undefined, 2));
});



exports.listFamilies = function (req, res)  {


        mysqlConnection.query('SELECT idFamily, last1, last2 FROM family', (err, rows, fields) => {
            if (!err)
                res.send(rows);
            else
                console.log(err);
        })
};


exports.addFamily=  function (req,res) {
	// Esto almacenara el cuerpo de la peticion post
    let fam = req.body;


    var sql = "SET @idFamily = ?;SET @last1 = ?;SET @last2 = ?;SET @qrCode = ?;  \
    CALL addOrEditFamily(@idFamily, @last1, @last2, @qrCode);";

    mysqlConnection.query(sql, [fam.idFamily, fam.last1, fam.last2, fam.qrCode], (err, rows, fields) => {
        if (!err)
            res.send(rows)
        else
            console.log(err);
    })
};

exports.getFamily = function (req, res)  {


        mysqlConnection.query('SELECT idFamily, last1, last2 FROM family where idFamily=?', 
            [req.params.idFamily],

            (err, rows, fields) => {
                if (!err)
                    res.send(rows);
                else
                    console.log(err);
            }
        )
};

exports.updateFamily = function (req,res) {
    // Esto almacenara el cuerpo de la peticion post
    let fam = req.body;


    var sql = "SET @idFamily = ?;SET @last1 = ?;SET @last2 = ?;SET @qrCode = ?;  \
    CALL addOrEditFamily(@idFamily, @last1, @last2, @qrCode);";

    mysqlConnection.query(sql, [req.params.idFamily, fam.last1, fam.last2, fam.qrCode], (err, rows, fields) => {
        if (!err)
            rows.forEach(

                element => {
                    if(element.constructor == Array)
                        res.send(element);
                }
            );
        else
            console.log(err);
    })
};

exports.deleteFamily = function (req, res)  {


        mysqlConnection.query('DELETE FROM family where idFamily=?', 
            [req.params.idFamily],  

            (err, rows, fields) => {
                if (!err)
                    res.send(rows);
                else
                    console.log(err);
            }
        )
};



exports.listKids = function (req, res)  {
        let qr= req.query.qr;


        if(!qr)
            mysqlConnection.query('SELECT idKid, names_, lasts_ FROM kid;',

             (err, rows, fields) => {
                    if (!err)
                        res.send(rows);
                    else
                        console.log(err);
                }
            )
        else 
            mysqlConnection.query('SELECT idKid, names_, lasts_ FROM kid INNER JOIN \
                family on kid.family_id = family.idFamily and family.qrCode= ?', [qr] ,

                (err, rows, fields)=> {
                    if (!err)
                        res.send(rows);
                    else
                        console.log(err);

                }

            )

};


exports.addKid =  function (req,res) {
    // Esto almacenara el cuerpo de la peticion post
    let kid = req.body;


    var sql = "SET @idKid = ?;SET @names_ = ?; SET @lasts_ = ?; SET @born_ = ?;\
    SET @level_ = ?; SET @photo = ?; SET @family = ?; \
    CALL addOrEditKid(@idKid, @names_, @lasts_ , @born_ ,@level_ , @photo, @family );";

    mysqlConnection.query(sql, [kid.idKid, kid.names_, kid.lasts_, kid.born_, kid.level_, kid.photo, kid.family], (err, rows, fields) => {
        if (!err)
            rows.forEach(

                element => {
                    if(element.constructor == Array)
                        res.send(element);
                }
            );
        else
            console.log(err);
    })
};



exports.getKid= function (req, res)  {


        mysqlConnection.query('SELECT * FROM kid where idKid=?', 
            [req.params.idKid],

            (err, rows, fields) => {
                if (!err)
                    res.send(rows);
                else
                    console.log(err);
            }
        )
};


exports.updateKid =  function (req,res) {
    // Esto almacenara el cuerpo de la peticion post
    let kid = req.body;


    var sql = "SET @idKid = ?;SET @names_ = ?; SET @lasts_ = ?; SET @born_ = ?;\
    SET @level_ = ?; SET @photo = ?; SET @family = ?; \
    CALL addOrEditKid(@idKid, @names_, @lasts_ , @born_ ,@level_ , @photo, @family );";

    mysqlConnection.query(sql, [req.params.idKid, kid.names_, kid.lasts_, kid.born_, kid.level_, kid.photo, kid.family], 
        (err, rows, fields) => {
        if (!err)
            rows.forEach(

                element => {
                    if(element.constructor == Array)
                        res.send(element);
                }
            );
        else
            console.log(err);
    })
};

exports.deleteKid = function (req, res)  {


        mysqlConnection.query('DELETE FROM kid where idKid=?', 
            [req.params.idKid],  

            (err, rows, fields) => {
                if (!err)
                    res.send(rows);
                else
                    console.log(err);
            }
        )
};



exports.addAssistence = function (req, res ) {

        let ids=  req.body;

        if(ids)
            mysqlConnection.query('SET @idKids=?;SET @times=?; call addAssitence(@idKids,@times);', 
                [ids.list, ids.count],  

                (err, rows, fields) => {
                    if (!err)
                        res.send("Exito agregando asistencia");
                    else
                        console.log(err);
                }
            )
        else
            res.send('Error agregando asistencia.');

};

exports.getAssistence = function (req, res) {

        mysqlConnection.query('select DATE_FORMAT(asistencia.fecha, "%d %M") as fecha ,\
                            CONCAT ( kid.names_ , ",", kid.lasts_) as nombre \
                            FROM asistencia inner join kid on asistencia.kid= kid.idKid\
                            order by fecha desc;' ,

                                         (err, rows, fields) => {
                                            if (!err)
                                                res.send(rows);
                                            else{
                                                res.send("Error en consulta.");
                                                console.log(err);
                                            }
                                        }
                                    

                        )
};